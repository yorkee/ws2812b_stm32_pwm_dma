# 使用STM32的PWM和DMA驱动WS2812B

#### 介绍

1. 硬件：[STM32F103C8T6核心板](https://item.taobao.com/item.htm?spm=a1z09.2.0.0.13712e8datvDhS&id=531633085101&_u=o55pts92deb): TIM3_CH1(PA6)
2. 软件：使用STM32CubeMX生成项目文件

#### 注意事项

1. 驱动逻辑电平要匹配，详见[参考手册](https://www.seeedstudio.com/document/pdf/WS2812B%20Datasheet.pdf)


#### 参考文章

1. [STM32F103+HAL+PWM+DMA+WS2812](https://blog.csdn.net/ben392797097/article/details/78075699)
2. [CHEATING AT 5V WS2812 CONTROL TO USE 3.3V DATA](https://hackaday.com/2017/01/20/cheating-at-5v-ws2812-control-to-use-a-3-3v-data-line/)
3. [STM32 interface WS2812B](http://fabioangeletti.altervista.org/blog/stm32-interface-ws2812b/?doing_wp_cron=1528043483.7364630699157714843750)



#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)